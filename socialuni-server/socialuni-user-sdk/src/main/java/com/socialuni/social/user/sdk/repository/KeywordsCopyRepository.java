package com.socialuni.social.user.sdk.repository;

import com.socialuni.social.user.sdk.model.DO.keywords.KeywordsCopyDO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author qinkaiyuan
 * @date 2018-10-17 21:59
 */
public interface KeywordsCopyRepository extends JpaRepository<KeywordsCopyDO, Integer> {

}


