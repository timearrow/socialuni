package com.socialuni.social.sdk.model.QO;

import lombok.Data;

@Data
public class SocialStringQO {
    private Integer text;
}