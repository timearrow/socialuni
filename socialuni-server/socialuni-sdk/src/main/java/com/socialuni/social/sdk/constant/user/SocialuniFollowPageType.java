package com.socialuni.social.sdk.constant.user;

/**
 * @author qinkaiyuan
 * @date 2020-03-07 18:32
 */
public class SocialuniFollowPageType {
    public static final String follow = "关注";
    public static final String fans = "粉丝";
}
